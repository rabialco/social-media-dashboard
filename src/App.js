import React, { useState, useEffect } from 'react';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { Link } from 'react-router-dom';
import Users from "./pages/users"
import Albums from "./pages/albums"
import Posts from "./pages/posts"
import UserDetail from './pages/userDetail';
import PostDetail from './pages/postDetail';
import AlbumDetail from './pages/albumDetail';
import PhotoDetail from './pages/photoDetail';
import './App.css';

function App() {
  return (
    <Router>
      <div>
        <li>
          <Link to="/">Home</Link>
        </li>
        <li>
          <Link to="/users">Users</Link>
        </li>
        <li>
          <Link to="/albums">Albums</Link>
        </li>
      </div>
      <Switch>
        <Route path='/' exact component={Posts} />
        <Route path='/users' component={Users} />
        <Route path='/albums' component={Albums} />
        <Route path='/user/detail/:id' component={UserDetail} />
        <Route path='/post/detail/:id' component={PostDetail} />
        <Route path='/album/detail/:id' component={AlbumDetail} />
        <Route path='/photo/detail/:id' component={PhotoDetail} />
      </Switch>
    </Router>
  );
}

export default App;
