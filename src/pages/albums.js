import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import '../App.css';

const Albums = () => {
  const [albums, setAlbums] = useState([]);
  useEffect(() => {
    const albumsAPI = 'https://jsonplaceholder.typicode.com/albums'

    const getAlbums = axios.get(albumsAPI)
    axios.all([getAlbums]).then(
      axios.spread((...data) => {
        const albumsData = data[0].data
        setAlbums(albumsData)
      })
    )

  }, [])
  return (
    <div className="Albums">
      <h2>List of Albums</h2>
      <ul>
        {albums.map(album => (
        <li>
          <Link to={`album/detail/${album.id}`}>
            {album.title}
          </Link>
        </li>
        ))}
      </ul>
    </div>
  );
}

export default Albums;
