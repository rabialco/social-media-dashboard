import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import '../App.css';

const Posts = () => {
  const [posts, setPosts] = useState([]);
  useEffect(() => {
    const postsAPI = 'https://jsonplaceholder.typicode.com/posts'

    const getPosts = axios.get(postsAPI)
    axios.all([getPosts]).then(
      axios.spread((...data) => {
        const postsData = data[0].data
        setPosts(postsData)
      })
    )

  }, [])
  return (
    <div className="Posts">
      <h2>List of Posts</h2>
      <ul>
        {posts.map(post => (
        <li>
          <Link to={`post/detail/${post.id}`}>
            {post.title}
          </Link>
          {post.name}
        </li>
        ))}
      </ul>
    </div>
  );
}

export default Posts;
