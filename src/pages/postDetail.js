import React, { useState, useEffect } from 'react';
import axios from 'axios';

const UserDetail = props => {
  var id = props.match.params.id
  const [postDetail, setPostDetail] = useState([]);
  const [comments, setComments] = useState([]);
  useEffect(() => {
    const postsDetailAPI = 'https://jsonplaceholder.typicode.com/posts/' + id
    const commentsAPI = 'https://jsonplaceholder.typicode.com/comments?postId=' + id

    const getPostDetail = axios.get(postsDetailAPI)
    const getComments = axios.get(commentsAPI)
    axios.all([getPostDetail, getComments]).then(
      axios.spread((...data) => {
        const postDetailData = data[0].data
        const commentsData = data[1].data
        setPostDetail(postDetailData)
        setComments(commentsData)
      })
    )

  }, [])


  return(
    <div>
        <h1>Post Details</h1>
        Post ID: {id} <br/>
        User ID: {postDetail.userId} <br/>
        Title : {postDetail.title} <br/>
        Body : {postDetail.body} <br/>
        <br/>
        <h3>Comments</h3>
        {comments.map(comment => (
          <div>
            <li>
              From :{comment.email} <br/>
              Name : {comment.name} <br/>
              Body : {comment.body}
            </li>
            <br/>
          </div>
        ))}
    </div>
  );
}
export default UserDetail;
