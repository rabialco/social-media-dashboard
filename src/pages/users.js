import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import '../App.css';

const Users = () => {
  const [users, setUsers] = useState([]);
  useEffect(() => {
    const usersAPI = 'https://jsonplaceholder.typicode.com/users'

    const getUsers = axios.get(usersAPI)
    axios.all([getUsers]).then(
      axios.spread((...data) => {
        const usersData = data[0].data
        setUsers(usersData)
      })
    )

  }, [])
  return (
    <div className="Users">
      <h2>List of Users</h2>
      <ul>
        {users.map(user => (
        <li>
          <Link to={{pathname:`user/detail/${user.id}`, user: {user}}}>
            {user.name}
          </Link>
        </li>
        ))}
      </ul>
    </div>
  );
}

export default Users;
