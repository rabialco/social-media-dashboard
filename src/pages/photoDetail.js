import React, { useState, useEffect } from 'react';
import axios from 'axios';

const PhotoDetail = props => {
  var id = props.match.params.id
  const [photo, setPhoto] = useState([]);
  useEffect(() => {
    const photosAPI = 'https://jsonplaceholder.typicode.com/photos/' + id

    const getPhoto = axios.get(photosAPI)
    axios.all([getPhoto]).then(
      axios.spread((...data) => {
        const photoData = data[0].data
        setPhoto(photoData)
      })
    )

  }, [])


  return(
    <div>
        <h1>Photo Details</h1>
        {photo.title} <br/>
        <img src={photo.url}></img>
    </div>
  );
}
export default PhotoDetail;
