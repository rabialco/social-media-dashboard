import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';

const UserDetail = props => {
  var id = props.match.params.id
  const [userDetail, setUserDetail] = useState([]);
  const [userDetailAddress, setUserDetailAddress] = useState([]);
  const [userDetailCompany, setUserDetailCompany] = useState([]);
  const [posts, setPosts] = useState([]);
  const [albums, setAlbums] = useState([]);
  useEffect(() => {
    const usersDetailAPI = 'https://jsonplaceholder.typicode.com/users/' + id
    const postsAPI = 'https://jsonplaceholder.typicode.com/posts?userId=' + id
    const albumsAPI = 'https://jsonplaceholder.typicode.com/albums?userId=' + id

    const getUserDetail = axios.get(usersDetailAPI)
    const getPosts = axios.get(postsAPI)
    const getAlbums = axios.get(albumsAPI)
    axios.all([getUserDetail, getPosts, getAlbums]).then(
      axios.spread((...data) => {
        const userDetailData = data[0].data
        const userDetailAddressData = data[0].data.address
        const userDetailCompanyData = data[0].data.company
        const postsData = data[1].data
        const albumsData = data[2].data
        setUserDetail(userDetailData)
        setUserDetailAddress(userDetailAddressData)
        setUserDetailCompany(userDetailCompanyData)
        setPosts(postsData)
        setAlbums(albumsData)
      })
    )

  }, [])


  return(
    <div>
        <h1>User Details</h1>
        User ID: {id} <br/>
        Name : {userDetail.name} <br/>
        UserName : {userDetail.username} <br/>
        Email : {userDetail.email} <br/>
        Address : {userDetailAddress.street}, {userDetailAddress.suite}, {userDetailAddress.city}, {userDetailAddress.zipcode} <br/>
        Phone : {userDetail.phone} <br/>
        Website : {userDetail.website} <br/>
        Company : {userDetailCompany.name} <br/>

        <h3>User Posts</h3>
        {posts.map(post => (
          <div>
            <li>
              Title :
              <Link to={`/post/detail/${post.id}`}>
                {post.title}
              </Link> <br/>
              Body : {post.body} <br/>
            </li>
            <br/>
          </div>
        ))}

      <h3>User Albums</h3>
        {albums.map(album => (
          <div>
            <li>
              Title :
              <Link to={`/album/detail/${album.id}`}>
                {album.title}
              </Link> <br/>
            </li>
            <br/>
          </div>
        ))}
    </div>
  );
}
export default UserDetail;
