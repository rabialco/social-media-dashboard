import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';

const AlbumDetail = props => {
  var id = props.match.params.id
  const [albumDetail, setAlbumDetail] = useState([]);
  const [photos, setPhotos] = useState([]);
  useEffect(() => {
    const albumsDetailAPI = 'https://jsonplaceholder.typicode.com/albums/' + id
    const photosAPI = 'https://jsonplaceholder.typicode.com/photos?albumId=' + id

    const getAlbumDetail = axios.get(albumsDetailAPI)
    const getPhotos = axios.get(photosAPI)
    axios.all([getAlbumDetail, getPhotos]).then(
      axios.spread((...data) => {
        const albumDetailData = data[0].data
        const photos = data[1].data
        setAlbumDetail(albumDetailData)
        setPhotos(photos)
      })
    )

  }, [])


  return(
    <div>
        <h1>Album Details</h1>
        Album ID: {id} <br/>
        Name : {albumDetail.title} <br/>
        <h3>Photo List</h3>
        {photos.map(photo => (
          <div>
            <li>
              Title : {photo.title} <br/>
              <Link to={`/photo/detail/${photo.id}`}>
                <img src={photo.thumbnailUrl}></img>
              </Link>
            </li>
            <br/>
          </div>
        ))}
    </div>
  );
}
export default AlbumDetail;
