import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';

ReactDOM.render(
  // const [users, setUsers] = useState([]);
  // const [posts, setPosts] = useState([]);
  // const [albums, setAlbums] = useState([]);
  // const [comments, setComments] = useState([]);
  // const [photos, setPhotos] = useState([]);
  // useEffect(() => {
  //   const usersAPI = 'https://jsonplaceholder.typicode.com/users'
  //   const postsAPI = 'https://jsonplaceholder.typicode.com/posts'
  //   const albumsAPI = 'https://jsonplaceholder.typicode.com/albums'
  //   const commentsAPI = 'https://jsonplaceholder.typicode.com/comments'
  //   const photosAPI = 'https://jsonplaceholder.typicode.com/photos'

  //   const getUsers = axios.get(usersAPI)
  //   const getPosts = axios.get(postsAPI)
  //   const getAlbums = axios.get(albumsAPI)
  //   const getComments = axios.get(commentsAPI)
  //   const getPhotos = axios.get(photosAPI)
  //   axios.all([getUsers, getPosts, getAlbums, getComments, getPhotos]).then(
  //     axios.spread((...data) => {
  //       const usersData = data[0].data
  //       const postsData = data[1].data
  //       const albumsData = data[2].data
  //       const commentsData = data[3].data
  //       const photosData = data[4].data
  //       setUsers(usersData)
  //       setPosts(postsData)
  //       setAlbums(albumsData)
  //       setComments(commentsData)
  //       setPhotos(photosData)
  //     })
  //   )

  // }, [])
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
  // <div className="App">
  //     <h2>User name</h2>
  //     <ul>
  //       {users.map(user => (
  //       <li key={user.id}>
  //         {user.name}
  //       </li>
  //       ))}
  //     </ul>
  //     ---
  //     <h2>Post Title</h2>
  //     <ul>
  //       {posts.map(post => (
  //       <li key={post.id}>
  //         {post.title}
  //       </li>
  //       ))}
  //     </ul>
  //     ---
  //     <h2>Album Title</h2>
  //     <ul>
  //       {albums.map(album => (
  //       <li key={album.id}>
  //         {album.title}
  //       </li>
  //       ))}
  //     </ul>
  //     ---
  //     <h2>Comment Title</h2>
  //     <ul>
  //       {comments.map(comment => (
  //       <li key={comment.id}>
  //         {comment.name}
  //       </li>
  //       ))}
  //     </ul>
  //     ---
  //     <h2>Photo Title</h2>
  //     <ul>
  //       {photos.map(photo => (
  //       <li key={photo.id}>
  //         {photo.title}
  //       </li>
  //       ))}
  //     </ul>
  //   </div>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
